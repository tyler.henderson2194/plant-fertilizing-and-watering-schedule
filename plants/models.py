from django.db import models

class Plants(models.Model):
    name = models.CharField(max_length=100)
    nickname = models.Charfield(blank=True)
    owner = models.ForeignKey(
        User,
        related_name="plants",
        on_delete=models.CASCADE,
        null=True
    )
